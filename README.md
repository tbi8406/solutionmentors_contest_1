Уявіть, що є дерев‘яні сходи. Коли деякі сходинки починають скрипіти (виходять з ладу), для ремонту їх готують дещо незвичайним чином.
Спеціальний робітник повинен занотувати на листку список сходинок, поставивши деякі відмітки в наступний спосіб:
Він повинен записати на листку про кожну сходинку по порядку: або "X" - якщо сходинка потребує ремонту, або "N" - число сходинок, що потребують ремонту по два боки від даної (враховуючи лише сусідні). Цифри ставляться тільки на справних сходинках. Якщо по сусідству зі справною сходинкою немає скрипучих (несправних) сходинок, то ставиться 0.
Один з таких робітників перед тим як віддати нотатки потрапив під дощ, та на листку потекли чорнила.
Ми не просимо вас відновити те, що було написано. Але пропонуємо вам написати програму для розрахунку кількості можливих варіантів того, що могло бути.
Для цього ми дамо вам рядок, де злитно буде написано те, що залишилось від нотаток. Де формально "?" - позначає те, що неможливо розібрати те, який символ був написаний. "X" - якщо у нотатках зберіглася інформація про те, чи зламана схода. "N" - якщо у нотатках зберіглася інформація про те, яка кількість сходинок потребує ремонту по обидва боки від даної (враховуючи лише сусідні). А у якості результату хочемо отримати єдине число - кількість можливих варіантів того, якою могла бути послідовність.
Наприклад:
?01???
Вивести треба буде:
4
Адже можливі конфігурації будуть:
001XX1;
001XXX;
001X2X;
001X10;
Або такий приклад введення:
??????0????????????01?
Чи навіть такий:
?2X?2X??1X2XX?2X1???X2???XX??X
Оскільки з ростом довжини вхідної послідовності, число варіантів зростає нелінійно, то обмежимо довжину вхідної послідовності 30ю символами.


Imagine a wooden staircase. When some of the steps are getting squeaky (damaged), they are prepared for repair in an unusual way.
A special worker has to write down a list of steps by putting some marks like that:
he must write about every step one by one: "X" - when a step needs repair, "N" - the number of steps that need a repair on both sides from the given one (considering the nearby ones only). The numbers are to put on the steps in a good condition. If there are no squeaky (damaged) steps near the good one, he puts 0.
One of the workers had been caught in the rain before he managed to pass his notes. All of his notes were damaged with the leaking ink. We do not ask you to restore what was written. But we suggest you write a program to calculate the number of possible options for what could there be.
So, we are giving you one line where everything that's left is written together. "?" means something that can't be read, "X" - if the notes contain any information on damaged stairs. "N" - if the notes contain any information on the number of stairs that need a repair on both sides from the given one (considering the nearby ones only). As a result, we'd like to see the number of sequences' possible variations.
For example:
?01???
You need to output:
4
As the possible configurations will be:
001XX1;
001XXX;
001X2X;
001X10;
Or we have another example of input:
??????0????????????01?
Or even something like that:
?2X?2X??1X2XX?2X1???X2???XX??X
So, the amount of variations, along with the increase of the input sequence, grows in a nonlinear way. Let's limit the length of the input sequence by 30 characters.
Try it!