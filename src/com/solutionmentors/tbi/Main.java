package com.solutionmentors.tbi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String staircase = br.readLine();
        System.out.println(calcNumberCases(staircase));
    }

    private static boolean isN(char step) {
        return step == '0' || step == '1' || step == '2' || step == 'N';
    }

    static int calcNumberCases(String stepsNotes) {
        char[] steps = ("NN" + stepsNotes + "NN").toCharArray();
        int countUndefinedSteps = 0;
        for (int i = 2; i < steps.length - 2; i++) {
            if (steps[i] != '?') continue;
            if (steps[i - 1] == '2' || steps[i + 1] == '2' || steps[i - 1] == '1' && isN(steps[i - 2]) || steps[i + 1] == '1' && isN(steps[i + 2])) {
                steps[i] = 'X';
            } else if (steps[i - 1] == '0' || steps[i + 1] == '0' || steps[i - 1] == '1' && steps[i - 2] == 'X' || steps[i + 1] == '1' && steps[i + 2] == 'X') {
                steps[i] = 'N';
            } else if (steps[i + 1] == '1' && steps[i + 2] == '?') {
                steps[i] = '-';
            } else {
                countUndefinedSteps++;
            }
        }
        return 1 << countUndefinedSteps;
    }
}
