package com.solutionmentors.tbi;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static com.solutionmentors.tbi.Main.calcNumberCases;
import static org.junit.Assert.assertEquals;

public class Tests {

    @Test
    public void fromProblem() {
        assertEquals(4, calcNumberCases("?01???"));
    }

    @Test()
    public void fromProblem2() {
        assertEquals(1024 * 32, calcNumberCases("??????0????????????01?"));
        assertEquals(128, calcNumberCases("?2X?2X??1X2XX?2X1???X2???XX??X"));
    }

    @Test
    public void minSize() {
        assertEquals(1, calcNumberCases("0"));
        assertEquals(1, calcNumberCases("X"));
        assertEquals(2, calcNumberCases("?"));
    }

    @Test
    public void maxSize() {
        assertEquals(1024 * 1024 * 1024, calcNumberCases("??????????????????????????????"));
    }

    @Test
    public void simpleCases() {
        assertEquals(1, calcNumberCases("?0?"));
        assertEquals(1, calcNumberCases("?2?"));
        assertEquals(4, calcNumberCases("??0??"));
        assertEquals(4, calcNumberCases("??2??"));
        assertEquals(1, calcNumberCases("?11?"));
        assertEquals(1, calcNumberCases("?10?"));
        assertEquals(2, calcNumberCases("?1X?"));
    }

    @Test
    public void oneAndUndefined() {
        assertEquals(2, calcNumberCases("?1?"));
        assertEquals(1, calcNumberCases("?1?1"));
        assertEquals(2, calcNumberCases("?1?1?"));
        assertEquals(1, calcNumberCases("1?1?1?"));
        assertEquals(1, calcNumberCases("1?1?1?1"));
    }

    private String numToTestString(int strLen, int num) {
        int[] steps = new int[strLen];
        for (int i = 0; i < strLen; i++, num >>= 1) {
            if ((num & 1) == 1) {
                steps[i] += 10;
                if (i > 0) {
                    steps[i - 1] += 1;
                }
                if (i < strLen - 1) {
                    steps[i + 1] += 1;
                }
            }
        }

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < strLen; i++) {
            switch (steps[i]) {
                case 0:
                    result.append('0');
                    break;
                case 1:
                    result.append('1');
                    break;
                case 2:
                    result.append('2');
                    break;
                default:
                    result.append('X');
                    break;
            }
        }
        return result.toString();
    }

    private String applyRain(String steps, int rain) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < steps.length(); i++, rain >>= 1) {
            if ((rain & 1) == 1) {
                result.append("?");
            } else {
                result.append(steps.charAt(i));
            }
        }
        return result.toString();
    }

    @Test
    public void allCasesLength3() {
        Map<String, Integer> cases = new HashMap<>();

        for (int i = 0; i < 8; i++) {
            String steps = numToTestString(3, i);
            for (int j = 0; j < 8; j++) {
                String afterRain = applyRain(steps, j);
                Integer casesCnt = cases.get(afterRain);
                cases.put(afterRain, casesCnt == null ? 1 : casesCnt + 1);
            }
        }

        int allCases = 0;
        for (Map.Entry<String,Integer> afterRain : cases.entrySet()) {
            allCases += afterRain.getValue();
            assertEquals(afterRain.getValue().intValue(), calcNumberCases(afterRain.getKey()));
        }
        assertEquals(64, allCases);
    }

    @Test
    public void allCasesLength6() {
        Map<String, Integer> cases = new HashMap<>();

        for (int i = 0; i < 64; i++) {
            String steps = numToTestString(6, i);
            for (int j = 0; j < 64; j++) {
                String afterRain = applyRain(steps, j);
                Integer casesCnt = cases.get(afterRain);
                cases.put(afterRain, casesCnt == null ? 1 : casesCnt + 1);
            }
        }

        int allCases = 0;
        for (Map.Entry<String,Integer> afterRain : cases.entrySet()) {
            allCases += afterRain.getValue();
            assertEquals(afterRain.getValue().intValue(), calcNumberCases(afterRain.getKey()));
        }
        assertEquals(4096, allCases);
    }
}
